import random
import time
import numpy as np
import cv2
import Client

"""
    Device Parent Class is censored.
"""

class Device(Client, object):
    def __init__(self, mySerialno, additionDelay=0):
        super(Device, self).__init__(serialno=mySerialno)
        self.additionDelay = additionDelay
        self.screenShot = None
    
    def tab(self, x, y, wait=0):
        if wait > 0:
            time.sleep(wait)
        self.touch(x+random.randint(-2, 2), y+random.randint(-2, 2))

    def tabSkip(self, wait=0):
        if wait > 0:
            time.sleep(wait)
        self.touch(200+(random.randint(-5, 5)), (random.randint(-5, 5)))

    def keyBack(self, wait=0):
        if wait > 0:
            time.sleep(wait)
        self.press(b'BACK')
    
    def takeScreenshot(self, wait=0):
        if wait > 0:
            time.sleep(wait)
        time.sleep(self.additionDelay)
        self.screenShot = self.takeSnapshot(reconnect=self.reconnect)        
        self.screenShot = np.array(self.screenShot)
        # Gray image.
        self.screenShot = cv2.cvtColor(self.screenShot, cv2.COLOR_RGBA2GRAY)

        return self.screenShot

    def takeScreenshotColor(self, wait):
        if wait > 0:
            time.sleep(wait)
        time.sleep(self.additionDelay)
        self.screenShot = self.takeSnapshot(reconnect=self.reconnect)        
        self.screenShot = np.array(self.screenShot)
        # BGR image for openCV.        
        self.screenShot = cv2.cvtColor(self.screenShot, cv2.COLOR_RGBA2BGR)

        return self.screenShot

    # For pick monsters in storage to replace max-level monsters
    def dragUp(self, x0_y0, x1_y1, duration=100, steps=1, orientation=-1, wait=0):
        (x0, y0) = x0_y0
        (x1, y1) = x1_y1
        self.drag((x0+random.randint(-2, 2), y0+random.randint(-2, 2)), (x0+x1+random.randint(-2, 2), y0+y1+random.randint(-2, 2)), duration=100, steps=1, orientation=-1)
