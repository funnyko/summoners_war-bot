from Images import Images

class Region(object):
    def __init__(self, region, template=None):
        self.region = region
        self.template = template
        
class Regions(object):
    # ========================
    # Regions for resolution
    #      1280x720
    # ========================
    def __init__(self, images):
        self.screenShot              = Region((0,0,1280,720))

        self.upperRight              = Region((950,0,250,220))
        """
                ...
                ...
                ...
             censored
        """
        self.statsSection            = Region((1080,200,200,300))

        # StageList
        self.battleGearWheelReg      = Region((0,610,113,111), images.battleGearWheel)
        self.victoryDiamondReg       = Region((900,290,175,150), images.victoryDiamond)
        self.victoryRaidDamageReg    = Region((85,305,185,60), images.victoryRaidDamage)
        """
                ...
                ...
                ...
             censored
        """
        
        # Rift World
        self.joinRiftWorld           = Region((880,568,62,37), images.joinRiftWorld)
        """
                ...
                ...
                ...
             censored
        """

        # Ally
        self.TopMon                  = Region((246,116,160,160))
        self.LeftMon                 = Region((110,188,160,160))
        """
                ...
                ...
                ...
             censored
        """

        # Arena
        self.eTopMon                 = Region((880,112,150,150))
        self.eLeftMon                = Region((750,185,150,150))
        """
                ...
                ...
                ...
             censored
        """

        # Levelling
        self.NewFodder               = Region((0,390,900,190))
        self.EndofMonL               = Region((830,390,200,230))
        """
                ...
                ...
                ...
             censored
        """
        """
                ...
                ...
                ...
             censored
        """



        # Rune Evaluatiol
        self.runeStarPoi             = Region((243,432,0,0))
        """
                ...
                ...
                ...
             censored
        """