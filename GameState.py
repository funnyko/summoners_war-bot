from enum import Enum

class GameDungeonState(Enum):
    PRE_GAME = 0
    IN_GAME = 1
    VICTORY = 2
    DEFEAT = 3
