from enum import Enum
import cv2
from Image import ImageUtils

class RuneRarity(Enum):
    NORMAL = 0
    MAGIC = 1
    RARE = 2
    HERO = 3
    LEGEND = 4

class RuneStar(Enum):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6

class RuneUtils():
    @staticmethod
    def getRuneStar(img, region):
        xShift = 11
        starCount = 0
        for i in range(6):
            poi = img[region[0], region[1]+(i*xShift)]
            if ImageUtils.pixelMatchesColor(poi, (239, 240, 238), 20):
                starCount += 1
        return starCount

    @staticmethod
    def getRuneRarity(img, region):
        poi = img[region[0], region[1]]
        rarity = None
        if ImageUtils.pixelMatchesColor(poi, (5, 33, 117), 20):
            rarity = RuneRarity.LEGEND
        elif ImageUtils.pixelMatchesColor(poi, (67, 17, 95), 20):
            rarity = RuneRarity.HERO
        elif ImageUtils.pixelMatchesColor(poi, (77, 66, 9), 20):
            rarity = RuneRarity.RARE
        return rarity
