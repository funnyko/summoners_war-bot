import cv2
class Image:
    def __init__(self, fileInfos, threshold, isGrayMode=True):
        fileName, mode = fileInfos[0], fileInfos[1]
        self.img = cv2.imread(fileName, mode) if mode is not None else cv2.imread(fileName)
        self.fileName = fileName
        # Similarity threshold
        self.threshold = threshold

class ImageUtils:
    @staticmethod
    def pixelMatchesColor(BGR, expectedBGRColor, tolerance=0):
        # [censored...]

        return # result 