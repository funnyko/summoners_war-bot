import Rune
import time
import argparse
from Images import Images
from Regions import Regions
from Config import Config, RuneConfig
from Device import Device
from GameState import GameDungeonState
from RegionHandler import RegionHandler

"""
    Example for dungeon farming only.
"""
runsCount = 0
victoryCount = 0
defeatCount = 0

def battlePreperationRoutine():
    global runsCount

    runsCount = runsCount + 1
    device.takeScreenshot()
    regionHandler.existsClick(device.screenShot, regions.screenShot.region, images.yes, wait=1) # Click yes in "No Leadership skill" pop-up

def battleRoutine(device):
    # tap play btn.
    regionHandler.existsClick(device.screenShot, regions.playReg.region, regions.playReg.template)
    # tap screen if accidentally paused.
    regionHandler.existsClick(device.screenShot, regions.pauseReg.region, regions.pauseReg.template)
    time.sleep(3)

def victoryRoutine(device):
    global victoryCount
    victoryCount += 1
    print("victory :%d"%victoryCount)
    print("defeated :%d"%defeatCount)
    time.sleep(0.2)    
    device.tabSkip(1)

    device.takeScreenshot(wait=0.7)
    if regionHandler.existsClick(device.screenShot, regions.screenShot.region, images.ok) == False:
        device.takeScreenshot(wait=0.2)
        
        if regionHandler.exists(device.screenShot, regions.screenShot.region, images.get) == True:
            # takeScreenshot with color
            device.takeScreenshotColor()
            # rune evaluation
            stars = Rune.RuneUtils.getRuneStar(device.screenShot, regions.runeStarPoi.region)
            rarity = Rune.RuneUtils.getRuneRarity(device.screenShot, regions.runeRarityPoi.region)
            if stars > runeConfig.minRuneStar.value or (stars >= runeConfig.minRuneStar.value and rarity.value >= runeConfig.minRuneRarity.value):
                device.keyBack(wait=0.2)
            else:
                # sell rune
                print('sell rune')
                device.takeScreenshot()
                if regionHandler.existsClick(device.screenShot, regions.screenShot.region, images.sell) == True:
                    # confirm if the rune is 5-6 stars
                    device.takeScreenshot(wait=0.2)
                    regionHandler.existsClick(device.screenShot, regions.screenShot.region, images.yes)
        else:
            # another rewards
            device.takeScreenshot(wait=0.2)
            regionHandler.untilExistsClick(regions.screenShot.region, images.ok)
            
    replayRoutine(device)

def defeatRoutine(device):
    global defeatCount

    defeatCount += 1
    print("victory :%d"%victoryCount)
    print("defeated :%d"%defeatCount)
    device.tabSkip(1)
    replayRoutine(device)

def replayRoutine(device):
    device.takeScreenshot(wait=0.5)
    print("replay")
    regionHandler.existsClick(device.screenShot, regions.replayDungeon.region, regions.replayDungeon.template)
    
    device.takeScreenshot(wait=0.5)
    if regionHandler.exists(device.screenShot, regions.bigFlashReg.region, regions.bigFlashReg.template) == False:
        refillEnergyRoutine()

def refillEnergyRoutine():
    print("refill energy")
    regionHandler.untilExistsClick(regions.screenShot.region, images.yes)
    regionHandler.untilExistsClick(regions.screenShot.region, images.rechargeFlash)
    regionHandler.untilExistsClick(regions.screenShot.region, images.yesRecharge)
    device.keyBack(wait=3)
    device.keyBack(wait=0.5)
    replayRoutine(device)
    
def runGame(device):
    device.takeScreenshot()
    if regionHandler.existsClick(device.screenShot, regions.bigFlashReg.region, images.bigFlash):
        battlePreperationRoutine()
        time.sleep(10)
        # tap play btn
        battleRoutine(device)
        time.sleep(mainConfig.inGameAverageTime)
        return GameDungeonState.PRE_GAME
    elif regionHandler.exists(device.screenShot, regions.battleGearWheelReg.region, regions.battleGearWheelReg.template):
        battleRoutine(device)
        return GameDungeonState.IN_GAME
    elif regionHandler.existsClick(device.screenShot, regions.victoryDiamondReg.region, regions.victoryDiamondReg.template):
        victoryRoutine(device)
        return GameDungeonState.VICTORY
    elif regionHandler.exists(device.screenShot, regions.screenShot.region, images.defeatedDiamond) \
    and regionHandler.existsClick(device.screenShot, regions.defeatedNo.region, regions.defeatedNo.template):
        defeatRoutine(device)
        return GameDungeonState.DEFEAT


if __name__ == "__main__":  
    ap = argparse.ArgumentParser()
    ap.add_argument('-did', '--deviceid', help='Specified device id if you\'re connecting more than 1 device.', required=False, default='.*')
    ap.add_argument('-cd', '--capturedelay', help='Add delay to capture screenshot (Reduce power consumption on mobile device.)', required=False, type=int, default=0)
    ap.add_argument('-gd', '--gamedelay', help='Time use per game, It won\'t capture screenshot during game playing. (Reduce power consumption on mobile device.)', required=False, type=int, default=20)
    args = vars(ap.parse_args())

    if args['deviceid'] is not None:
        deviceId = args['deviceid']
    if args['capturedelay'] is not None:
        timeFreq = args['capturedelay']
    if args['gamedelay'] is not None:
        inGameAverageTime = args['gamedelay']

    runeConfig = RuneConfig(
        minRuneStar = Rune.RuneStar.SIX, 
        minRuneRarity = Rune.RuneRarity.RARE, 
    )

    mainConfig = Config(
        inGameAverageTime = inGameAverageTime,
        isRefillEnergy=True,
        isDebug=True,
        timeFreq=timeFreq
    )

    device = Device(deviceId, mainConfig.timeFreq)
    images = Images("../imgs/")
    regions = Regions(images)
    regionHandler = RegionHandler(device)
 
    while(1):
        tStart = time.time()
        gameState = runGame(device)
        print('end time:', time.time()-tStart)