import Rune

class Config:
    def __init__(self=True, inGameAverageTime=60, isRefillEnergy=True, isDebug=True, timeFreq=50):
        self.inGameAverageTime = inGameAverageTime
        self.isRefillEnergy = isRefillEnergy
        self.isDebug = isDebug
        self.timeFreq = timeFreq


class RuneConfig:
    def __init__(self, minRuneStar=5, minRuneRarity=Rune.RuneRarity.RARE, isKeepRunePrimeHP=True, isKeepRunePrimeATK=True, isKeepRunePrimeDEF=True, isKeepRunePrimeSPD=True, isKeepRunePrimeCRIRate=True, isKeepRunePrimeCRIDmg=True, isKeepRunePrimeRES=True, isKeepRunePrimeACC=True):
        self.minRuneStar = minRuneStar
        self.minRuneRarity = minRuneRarity
        self.isKeepRunePrimeHP = isKeepRunePrimeHP
        self.isKeepRunePrimeATK = isKeepRunePrimeATK
        self.isKeepRunePrimeDEF = isKeepRunePrimeDEF
        self.isKeepRunePrimeSPD = isKeepRunePrimeSPD
        self.isKeepRunePrimeCRIRate = isKeepRunePrimeCRIRate
        self.isKeepRunePrimeCRIDmg = isKeepRunePrimeCRIDmg
        self.isKeepRunePrimeRES = isKeepRunePrimeRES
        self.isKeepRunePrimeACC = isKeepRunePrimeACC
        self.isKeepRunePrimeSPD = isKeepRunePrimeSPD