import time
import cv2
import numpy as np

class RegionHandler():
    def __init__(self, device):
        self.device = device

    def matchTemplate(self, screenShot, region, template, wait=0):
        if template is None:
            raise ValueError('A very specific bad thing happened')

        if wait > 0:
            time.sleep(wait)
        roi = screenShot[region[1]:region[1]+region[3], region[0]:region[0]+region[2]]

        res = cv2.matchTemplate # censored
        loc = # censored
        pts = list(zip(*loc[::-1]))

        if len(pts) > 0:
            return True, pts[0]
        else:
            return False, []

    # Check only.
    def exists(self, img, region, template, wait=0):
        template = template
        isExist, pt = self.matchTemplate(img, region, template, wait)
        return isExist
    
    # Check and then click if exists.
    def existsClick(self, img, region, template, wait=0):
        isExist, pt = self.matchTemplate(img, region, template, wait)
        if isExist:
            w, h = template.img.shape[::-1]
            self.device.tab(region[0]+(pt[0]+(w/2)), region[1]+(pt[1]+(h/2)))
        return isExist

    # Check and click with multi templates.
    def inArrExistsClickOne(self, img, region, templates, wait=0):
        isExist = False
        for template in templates:
            isExist, pt = self.matchTemplate(img, region, template, wait)
            if isExist:
                w, h = template.img.shape[::-1]
                self.device.tab(region[0]+(pt[0]+(w/2)), region[1]+(pt[1]+(h/2)))
                break
        return isExist

    # Check and take new screen until exists.
    def untilExists(self, region, template, wait=0):
        isExist = False
        while(isExist != True):
            screenShot = self.device.takeScreenshot()
            isExist, pt = self.matchTemplate(screenShot, region, template, wait)
        return True

    # Check and take new screen until exists then click if exists.
    def untilExistsClick(self, region, template, wait=0):
        isExist = False
        while(isExist != True):
            screenShot = self.device.takeScreenshot()
            isExist, pt = self.matchTemplate(screenShot, region, template, wait)
            if isExist:
                w, h = template.img.shape[::-1]
                self.device.tab(region[0]+(pt[0]+(w/2)), region[1]+(pt[1]+(h/2)))
        return True