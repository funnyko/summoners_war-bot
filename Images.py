from Image import Image

class Images:
    def __init__(self, folderName, language="en"):
        # General (commonly used)
        self.areaMap				= Image((folderName+"areaMap.png", 0), (0.9))
        self.battleButton			= Image((folderName+"battleButton.png", 0), (0.9))
        self.battleGearWheel		= Image((folderName+"battleGearWheel.png", 0), (0.7))
        self.cancel 				= Image((folderName+"cancel."+language+".png", 0), (0.8))
        """
                ...
                ...
                ...
             censored
        """
        # Monster Leveling and Opponent checking
        self.emptyMon				= Image((folderName+"emptyMon.png", 0), (0.8))
        """
                ...
                ...
                ...
             censored
        """
        # Scenario Area's
        self.areaAiden				= Image((folderName+"areaAiden.png", 0), (0.9))
        self.areaArena				= Image((folderName+"areaArena.png", 0), (0.9))
        """
                ...
                ...
                ...
             censored
        """

        # Dungeon Area's
        self.dungeonGiants			= Image((folderName+"dungeonGiants.png", 0), (0.9))
        self.dungeonDragons			= Image((folderName+"dungeonDragons.png", 0), (0.9))
        """
                ...
                ...
                ...
             censored
        """
        # Rune related
        self.runeStar				= Image((folderName+"runeStar.png", 0), (0.8))
        self.runeStar1				= Image((folderName+"runeStar1.png", 0), (0.9))
         """
                ...
                ...
                ...
             censored
        """
        # Rune upgrading related
        self.runePowerUpButton      = Image((folderName+"runePowerUpButton."+language+".png", 0), (0.84))
        self.runeLevel1             = Image((folderName+"runeLevel1.png", 0), (0.7))
        """
                ...
                ...
                ...
             censored
        """

        # Scenario / Dungeon / Raid related
        self.bigFlash				= Image((folderName+"bigFlash.png", 0), (0.8))
        self.scenarioFlash			= Image((folderName+"scenarioFlash.png", 0), (0.8))
        """
                ...
                ...
                ...
             censored
        """
        #endArea				= Image((folderName+"endArea.png", 0), (0.9))
        self.raiddmg				= Image((folderName+"raidDmg."+language+".png", 0), (0.7))
        self.requireEnergy0			= Image((folderName+"require0.png", 0), (0.9))
        """
                ...
                ...
                ...
             censored
        """
        # Arena related
        self.arenaDialog		    = Image((folderName+"arenaDialog.png", 0), (0.7))
        """
                ...
                ...
                ...
             censored
        """
        # Level
        self.maxLevel               = Image((folderName+"maxLevel.png", 0), (0.95))
        